library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Decoder_BCD_tb is
end;

architecture bench of Decoder_BCD_tb is

  component Decoder_BCD
    Port (
      entrada:in std_logic_vector(7 downto 0);
      S0,S1,S2,S3,S4,S5,S6,S7:out std_logic_vector(5 downto 0)
     );
  end component;

  signal entrada: std_logic_vector(7 downto 0);
  signal S0,S1,S2,S3,S4,S5,S6,S7: std_logic_vector(5 downto 0) ;

begin

  uut: Decoder_BCD port map ( entrada => entrada,
                              S0      => S0,
                              S1      => S1,
                              S2      => S2,
                              S3      => S3,
                              S4      => S4,
                              S5      => S5,
                              S6      => S6,
                              S7      => S7 );

  stimulus: process
  begin
  
    entrada <= "00000001";--numero 1
    wait for 1000 ns;
    entrada <= "00001010";--numero 10
    wait for 1000 ns;
    entrada <= "01011010";--numero 90
    wait for 1000 ns;
    entrada <= "01100100";--numero 100
    wait for 1000 ns;
    entrada <= "11001011";--numero 203/agua
     wait for 1000 ns;


    wait;
  end process;


end;
