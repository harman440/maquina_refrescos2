library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity TOP_tb is
end;

architecture bench of TOP_tb is

  component TOP
      Port ( 
    clk: in std_logic;
    cocacola,fanta,agua:in std_logic;
    reset:in std_logic;
    c10,c20,c50,euro1:in std_logic;
    display_number : out  STD_LOGIC_VECTOR (7 downto 0);
    display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
      );
  end component;

  signal clk,cocacola,fanta,agua,reset: STD_LOGIC;
  signal c10,c20,c50,euro1: STD_LOGIC;
  signal display_number,display_selection: STD_LOGIC_VECTOR(7 downto 0);
  
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;
begin

uut: TOP port map ( clk => clk,
                    cocacola => cocacola,
                    fanta => fanta,
                    agua => agua,
                    reset => reset,
                    c10 => c10,
                    c20 => c20,
                    c50 => c50,
                    euro1 => euro1,
                    display_number => display_number,
                    display_selection => display_selection );

  stimulus: process
  begin
  reset <= '0';
  cocacola<='0';
  fanta<='0';
  agua<='0';
  c10<='0';
  c20<='0';
  c50<='0';
  euro1<='0';
  wait for 1000ns;
  
  reset<='0';
  wait for 1000ms;
  
  c10 <= '1';
  wait for 1000ms;
  
  c10 <='0';
  wait for 1000ms;
  
  c50 <= '1';
  wait for 1000ms;
  
  c50 <='0';
  wait for 1000ms;
  
  
    stop_the_clock <= true;

  wait;
 end process;

clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
