library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity clk_divider_200hz_tb is
end;

architecture bench of clk_divider_200hz_tb is

  component clk_divider_200hz
      Generic (frec: integer:=124999);
      Port ( 
          clk:in std_logic;
          reset:in std_logic;
          clk_out:out std_logic
      );
  end component;

  signal clk: std_logic;
  signal reset: std_logic;
  signal clk_out: std_logic ;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: clk_divider_200hz 
                            port map ( clk     => clk,
                                       reset   => reset,
                                       clk_out => clk_out );

  stimulus: process
  begin
  

reset<='1';
wait for 100 ns;
reset<='0';
wait for 100 ms;


    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
