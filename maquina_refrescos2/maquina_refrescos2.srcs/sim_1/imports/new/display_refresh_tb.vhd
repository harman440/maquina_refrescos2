library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity display_refresh_tb is
end;

architecture bench of display_refresh_tb is

  component display_refresh
      Port ( clk : in  STD_LOGIC;
             s0,s1,s2,s3,s4,s5,s6,s7 : IN std_logic_vector(7 downto 0);
             display_number : out  STD_LOGIC_VECTOR (7 downto 0);
             display_selection : out  STD_LOGIC_VECTOR (7 downto 0));
  end component;

  signal clk: STD_LOGIC;
  signal s0,s1,s2,s3,s4,s5,s6,s7: std_logic_vector(7 downto 0);
  signal display_number: STD_LOGIC_VECTOR (7 downto 0);
  signal display_selection: STD_LOGIC_VECTOR (7 downto 0);

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: display_refresh port map ( clk               => clk,
                                  s0                => s0,
                                  s1                => s1,
                                  s2                => s2,
                                  s3                => s3,
                                  s4                => s4,
                                  s5                => s5,
                                  s6                => s6,
                                  s7                => s7,
                                  display_number    => display_number,
                                  display_selection => display_selection );

  stimulus: process
  begin

    s0<= x"C0";
    s1 <=x"F9";
    s2<=x"A4";
    s3<=x"C0";
    s4<=x"C0";
    s5<=x"C0";
    s6<=x"C0";
    s7<=x"C0";
    wait for 1000ns;


    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;