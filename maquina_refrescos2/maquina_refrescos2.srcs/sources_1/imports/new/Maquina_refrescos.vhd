
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Maquina_refrescos is
  Port ( 
  clk:in std_logic;
  dinerok:in std_logic_vector(1 downto 0);
  current_money: in std_logic_vector(7 downto 0);
  reset:in std_logic;
  cocacola,fanta,agua:in std_logic;
  salida: out std_logic_vector(7 downto 0);
  reset_money: out std_logic
  );
end Maquina_refrescos;


architecture behavioral of Maquina_refrescos is
type state_type is(S1,S2,S3,S4);
signal state, next_state:state_type;
signal bebida: std_logic_vector(7 downto 0);

begin
sync_proc: process (clk,reset)
begin
   if rising_edge(clk) then
        state <= next_state;
   end if;
end process;

output_decode:process(state,current_money,reset)
begin
    case(state)is
    when S1=> salida<=current_money;
              if (reset='1') then
                reset_money<='1';
              else
                reset_money<='0';
              end if;
    when S2=> salida<="11001101";--205--seleccion_beb;
              reset_money<='1';
    when S3=> salida<="11001100";--el numero 204 en binario que corresponde a error.
              reset_money<='1';
    when S4=> salida<=bebida;
              reset_money<='1';
    end case;
end process;
             
next_state_decode:process(state,dinerok,agua,cocacola,fanta,reset)
begin
    next_state<=state;
    case(state)is
    when S1=> -- estado pedir dinero
            if(dinerok = "01")then --"01" dinero clavado (1euro); "00" no se ha pasado; "10" dinero excesivo (>1?)
                next_state<=S2;
            elsif(dinerok = "10")then
                next_state<=S3;--S3 es el estado de error           
            elsif(dinerok = "00") then
                next_state<=S1;
            end if;
            
    when S2=>  -- estado pedir refresco
            if(cocacola='1')then
                bebida<="11001001";--201
            elsif(fanta='1')then
                bebida<="11001010";--202
            elsif(agua='1')then
                bebida<="11001011";--203
            end if;
            if(cocacola or fanta or agua)= '1' then
                next_state<=S4;
            elsif(reset = '1') then
                next_state<=S1;
            else
                next_state<=S2;
            end if;
    when S3=> --estado de error
        if (reset = '1') then
            next_state<=S1;
        else
            next_state<=S3;
        end if;   
    when S4=> --mostrar refresco seleccionado
        if (reset = '1') then
            next_state<=S1;
        else
            next_state<=S4;
        end if;   
    when others=>next_state<=S1;
            
    end case;   
end process;

end behavioral;
