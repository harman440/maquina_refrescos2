library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity money_counter is
  Port (
  clk: in std_logic;
  c10,c20,c50,euro1:in std_logic;
  reset_money_counter:in std_logic;
  current_money:out std_logic_vector(7 downto 0);--numero en binario de 0 a 200
  dinerok:out std_logic_vector(1 downto 0)
   );
end money_counter;

architecture Behavioral of money_counter is

signal dinero_ok:std_logic_vector(1 downto 0);
begin
process(c10,c20,c50,euro1, reset_money_counter,clk)
subtype dinero_t is integer range 0 to 200;
variable dinero_actual: dinero_t:=0;
begin
if rising_edge(clk) then
    if(reset_money_counter = '1') then
        dinero_actual :=0;
    elsif(c10='1')then 
        dinero_actual:= dinero_actual + 10;
    elsif(c20='1')then
        dinero_actual:= dinero_actual + 20;
    elsif(c50='1') then
        dinero_actual:= dinero_actual + 50;
    elsif(
    euro1='1') then
        dinero_actual:= dinero_actual + 100;
    end if;
end if;
    if(dinero_actual = 100)then
        dinero_ok <="01";
    elsif(dinero_actual<100)then
        dinero_ok <="00";
    else
        dinero_ok <="10";
    end if;
    current_money<=std_logic_vector(to_unsigned(dinero_actual, current_money'length));
end process;
dinerok<=dinero_ok;
end Behavioral;
